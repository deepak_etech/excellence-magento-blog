<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Maester
 */

get_header();
$maester_lite_enable_blog_thumbnail = get_theme_mod('enable_blog_thumbnail', true);
$maester_lite_enable_blog_category = get_theme_mod('enable_blog_category', true);
$maester_lite_enable_blog_content = get_theme_mod('enable_blog_content', true);
$maester_lite_enable_blog_author = get_theme_mod('enable_blog_author', true);
$maester_lite_enable_blog_date = get_theme_mod('enable_blog_date', true);
$maester_lite_enable_single_blog_navigation = get_theme_mod('enable_single_blog_navigation', true);
$maester_lite_enable_single_blog_comments = get_theme_mod('enable_single_blog_comments', true);
$maester_lite_enable_single_blog_sidebar = get_theme_mod('enable_single_blog_sidebar', true);
$maester_lite_blog_column_class = !$maester_lite_enable_single_blog_sidebar || !is_active_sidebar( 'sidebar-1' ) ? 'col-12' : 'col-lg-9';
maester_lite_breadcrumbs();
?>
    <div id="content" class="site-content">
        <div class="container">

            <div class="row detail-top-row">
                <div class="col-md-6">
                    <header class="entry-header">
                        <?php
                            /* translators: used between list items, there is a space after the comma */
                            $maester_lite_categories_list = get_the_category_list( ', ' );
                            if ( $maester_lite_enable_blog_category && $maester_lite_categories_list ) {
                                /* translators: 1: list of categories. */
                                printf( '<span class="cat-links">' . esc_html__( ' %1$s', 'maester-lite' ) . '</span>', wp_kses(
                                    $maester_lite_categories_list,
                                    array(
                                        "a" => array(
                                                "href" => array(),
                                                "rel" => array()
                                        )
                                    )
                                ) );
                            }
                            the_title( '<h3 class="entry-title">', '</h3>' );
                            while ( have_posts() ) :
                                the_post();
                                if ( 'post' === get_post_type() ) {
                                    echo "<div class='maester-single-post-meta'>";
                                    the_author();
                                    echo "</div>";
                                } 
                            endwhile;
                        ?>
                        <div class="publish_date">
                            <?php
                                echo get_the_date(' F j, Y');
                            ?>
                        </div>
                        <div class="row social_link">
                            <img src="https://plivo1.demo.xmagestore.com/excelblog/wp-content/uploads/2020/02/Blog-Detailed.png">
                            <img src="https://plivo1.demo.xmagestore.com/excelblog/wp-content/uploads/2020/02/Blog-Detailed-1.png">
                        </div>
                    </header><!-- .entry-header -->
                </div>
                <div class="col-md-6 detail-post-image">
                    <?php maester_lite_post_thumbnail(); ?>
                </div>
            </div><!-- .row -->

            <div class="row detail-row">
                <div class="<?php echo esc_attr($maester_lite_blog_column_class) ?> detail-content-part">
                    <div id="primary" class="content-area">
                        <main id="main" class="site-main">

                            <?php
                                while ( have_posts() ) :
                                    the_post();
                                    get_template_part( 'template-parts/content', 'single' );
                                endwhile; // End of the loop.
                            ?>

                        </main><!-- #main -->
                    </div><!-- #primary -->
                    <div class="realted_post">
                        <?php
                            $posts = array();
                            $categories = get_the_category($post->ID);
                            // echo "<pre>";
                            if (count($categories) > 0) {
                                foreach( $categories as $key => $category ){
                                    if( $category->count > 0 ){
                                        $args=array(
                                            'category__in' => array($category->term_id),
                                            'post__not_in' => array($post->ID),                
                                        );
                                        $my_query = new WP_Query($args);
                                        if( $my_query->have_posts() ){
                                            $posts = array_merge($posts, $my_query->get_posts());
                                        }
                                    }
                                }
                                //print_r($posts); //print related post array
                            }
                        ?>
                        <div class="row featured_post">
                            <h3>Realated Post</h3>
                        </div>
                        <div class="continue_underline"></div>
                        <div class="row">
                            <?php
                                $i=0;
                                foreach($posts as $post){
                                    $id=$post->ID;?>
                                    <div class="col-md-6">
                                        <?php
                                            get_template_part( 'template-parts/content', get_post_type() );
                                        ?>
                                    </div>
                                    <?php
                                    $i++;
                                    if($i==2) break;
                                }
                            ?>
                        </div>
                    </div>
                </div><!-- .col-lg-* -->
                <?php if($maester_lite_enable_single_blog_sidebar) get_sidebar(); ?>
            </div><!-- .row -->
            <div class="row">
                <div class="col-md-7">
                    <p class="provideInfoHeading">Please, provide us with as much detailed information as possible</p>
                    <?php echo do_shortcode("[wpforms id='66']"); ?>
                </div>
                <div class="col-md-5">
                
                    <p class="matchHeading">THINK WE’D BE A MATCH?</p>
                    <p class="heading">LET’S CONNECT</p>
                    <p class="subheading">Email</p>
                    <p class="para">contact@excellencetechnologies.in</p>
                    <p class="subheading">Address</p>
                    <p class="para">C86 B,Sector 8,Noida (Uttar Pradesh)</p>
                    <p class="subheading">Phone</p>
                    <p class="para">0120- 4278603</p>
    
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #content  -->
<?php
get_sidebar();
get_footer();