<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Maester
 */

get_header();
$maester_lite_enable_blog_thumbnail = get_theme_mod('enable_blog_thumbnail', true);
$maester_lite_enable_blog_category = get_theme_mod('enable_blog_category', true);
$maester_lite_enable_blog_content = get_theme_mod('enable_blog_content', true);
$maester_lite_enable_blog_author = get_theme_mod('enable_blog_author', true);
$maester_lite_enable_blog_date = get_theme_mod('enable_blog_date', true);
$maester_lite_enable_blog_sidebar = get_theme_mod('enable_blog_sidebar', true);
$maester_lite_post_column_count = get_theme_mod('post_column_count', '6');
$maester_lite_blog_column_class = !$maester_lite_enable_blog_sidebar || !is_active_sidebar( 'sidebar-1' ) ? 'col-12' : 'col-lg-9';
maester_lite_breadcrumbs();
?>
    <div id="content" class="site-content">
        <div class="container">
            <div class="row featured_main">
            <div class="row">
                <?php
                $args = array(
                        'posts_per_page' => 1,
                        'meta_key' => 'meta-checkbox',
                        'meta_value' => 'yes'
                    );
                    $featured = new WP_Query($args);
                
                if ($featured->have_posts()): while($featured->have_posts()): $featured->the_post(); ?>
                    <div class="col-md-6">
                        <div class="row featured_post">
                            <h3>Featured Post</h3>
                        </div>
                        <div class="continue_underline"></div>
                        <?php $maester_lite_categories_list = get_the_category_list( ', ' );
                        if ( $maester_lite_enable_blog_category && $maester_lite_categories_list ) {
                            printf( '<span class="cat-links">' . esc_html__( ' %1$s', 'maester-lite' ) . '</span>', wp_kses(
                                $maester_lite_categories_list,
                                array(
                                    "a" => array(
                                            "href" => array(),
                                            "rel" => array()
                                    )
                                )
                            ) );
                        }
                        the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
                        if ( 'post' === get_post_type() ) :
                            ?>
                            <div class="entry-meta">
                                <?php
                                    if($maester_lite_enable_blog_author) maester_lite_posted_by();echo get_the_date(' F j, Y');
                                ?>
                            </div><!-- .entry-meta -->
                        <?php endif;
                        ?>
                        <?php if($maester_lite_enable_blog_content) {
                            the_excerpt();
                        }; ?>
                        <a class="continue_reading" href='<?php echo esc_url( get_permalink() )?>'>Continue Reading</a>
                        <div class="continue_underline"></div>
                    </div>
                    <div class="col-md-6 feature_post_image">
                    <figure> <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> </figure>    
                    <!-- <?php if($maester_lite_enable_blog_thumbnail) maester_lite_post_thumbnail(); ?> -->
                    </div>
                <?php
                endwhile; else:
                endif;
                ?>
                </div>
            </div>
            <div class="row content_part_main">
                <div class="<?php echo esc_attr($maester_lite_blog_column_class) ?>">
                    <div id="primary" class="content-area">
                        <main id="main" class="site-main">

                            <?php
                            if ( have_posts() ) :

                                if ( is_home() && ! is_front_page() ) :
                                    ?>
                                    <header>
                                        <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                                    </header>
                                <?php
                                endif;
                                echo "<div class='row blog_post'>";
                                /* Start the Loop */
                                while ( have_posts() ) :
                                    the_post();

                                    /*
                                     * Include the Post-Type-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                                     */
                                    printf("<div class='col-md-%s post-column'>", esc_attr($maester_lite_post_column_count));
                                    get_template_part( 'template-parts/content', get_post_type() );
                                    printf("</div>"); // .col-md-*

                                endwhile;

                                echo "</div>"; // .row
                                load_more_button();

                            else :

                                get_template_part( 'template-parts/content', 'none' );

                            endif;
                            ?>

                        </main><!-- #main -->
                    </div><!-- #primary -->
                </div><!-- .col-md-* -->
                <?php if($maester_lite_enable_blog_sidebar) get_sidebar(); ?>
            </div><!-- .row -->
        </div><!-- .container -->

    </div><!-- #content -->

<?php

get_footer();
